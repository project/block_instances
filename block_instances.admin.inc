<?php

/**
 * @file
 * Admin page callbacks for the block module.
 */

function block_instances_advanced_settings() {
  $form = array();

  foreach (module_implements('block') as $module) {
    foreach (module_invoke($module, 'block', 'list') as $id => $block) {
      $blocks["$module:$id"] = filter_xss($block['info']);
    }
  }

  $form['block_instances_exlude_blocks'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Exclude blocks'),
    '#options' => $blocks,
    '#default_value' => variable_get('block_instances_exlude_blocks', array()),
    '#description' => t('The blocks selected here will not be availale to include in news block instances'),
  );

  $form['block_instances_region_conversor_string'] = array(
    '#type' => 'textarea',
    '#title' => t('Region conversor'),
    '#description' => t('Some Drupal themes contains special names for regions 
    like <em>left</em>, or <em>right</em> instead of <em>sidebar_first</em> and
     <em>sidebar_second</em>. Here you can define a what region have to be replaced.
    Use <strong>original_region|replace_with_region</strong> (i.e: left|sidebar_first) one conversion per line.'),
    '#default_value' => variable_get('block_instances_region_conversor_string', ''),
  );

  $form['#submit'][] = 'block_instances_advanced_settings_submit';

  return system_settings_form($form);
}

function block_instances_advanced_settings_submit($form, $form_state) {
  $region_conversor = array();
  if (!empty($form_state['values']['block_instances_region_conversor_string'])) {
    $str = str_replace("\n\r", "\n", $form_state['values']['block_instances_region_conversor_string']);
    $lines = explode("\n", $str);
    foreach ($lines as $conversion) {
      list($original, $replace) = explode('|', $conversion);
      $region_conversor[$original] = $replace;
    }
  }
  variable_set('block_instances_region_conversor', $region_conversor);
}

/**
 * Replace the default administration block interface with only a list of custom blocks.
 */
function block_instances_admin_blocks() {
  $res = db_query("SELECT * FROM {boxes}");
  while ($ob = db_fetch_object($res)) {
    $boxes[] = array(
      'title' => t('Block : @block', array('@block' => $ob->info)),
      'edit' => l(t('Configure'), 'admin/build/block/configure/block/' . $ob->bid),
      'delete' => l(t('Delete'), 'admin/build/block/delete/' . $ob->bid),
    );
  }

  if (module_exists('boxes')) {
    foreach (module_invoke('boxes', 'block', 'list') as $delta => $box) {
      $boxes[] = array(
        'title' => t('Box : @box', array('@box' => $box['info'])),
        'edit' => l(t('Configure'), 'admin/build/block/configure/boxes/' . $delta),
        'delete' => l(t('Delete'), 'admin/build/block/configure/boxes/' . $delta . '/delete'),
      );
    }
  }  
  $header = array(
    'title' => t('Block'),
    'operations' => array('data' => t('Operations'), 'colspan' => 2),
  );
  if (count($boxes)) {
    return theme_table($header, $boxes);
  }
  else {
    return t('There are not custom blocks created yet. !create_one.', array('!create_one' => l(t('Create one'), 'admin/build/block/add')));
  }
}

/**
 * Menu callback for admin/build/block.
 */
function block_instances_admin_display($theme = NULL) {
  global $custom_theme, $theme_key;

  // If non-default theme configuration has been selected, set the custom theme.
  $custom_theme = $theme_key =  isset($theme) ? $theme : variable_get('theme_default', 'garland');

  ctools_include('export');
  $blocks_instances = ctools_export_load_object('block_instances');

  // Sort the block instances by region and weight
  uasort($blocks_instances, '_sort_block_instances');

  $blocks = array();
  foreach ($blocks_instances as $blocks_instance) {
    list($module, $delta) = explode(':', $blocks_instance->settings['block']);
    $info = module_invoke($module, 'block', 'list');
    $blocks[] = array(
      'info' => $info[$delta]['info'],
      'cache' => $info[$delta],
      'module' => $module,
      'delta' =>  $delta,
      'theme' => $custom_theme,
      'status' => 1,
      'weight' => $blocks_instance->weight,
      'original_weight' => $blocks_instance->weight,
      'region' => $blocks_instance->region,
      'original_region' => $blocks_instance->region,
      'custom' => '',
      'block_instance' => $blocks_instance->machine_name,
      'title' =>  $info['info'],
    );
  }

  usort($blocks, '_block_instances_compare');

  return drupal_get_form('block_instances_admin_display_form', $blocks, $theme);
}

/**
 * Generate main blocks administration form.
 */
function block_instances_admin_display_form(&$form_state, $blocks, $theme = NULL) {
  global $theme_key, $custom_theme;

  // Add CSS
  drupal_add_css(drupal_get_path('module', 'block') .'/block.css', 'module', 'all', FALSE);

  // If non-default theme configuration has been selected, set the custom theme.
  $custom_theme = isset($theme) ? $theme : variable_get('theme_default', 'garland');
  init_theme();

  $throttle = module_exists('throttle');
  $block_regions = system_region_list($theme_key) + array(BLOCK_REGION_NONE => '<'. t('none') .'>');

  // Weights range from -delta to +delta, so delta should be at least half
  // of the amount of blocks present. This makes sure all blocks in the same
  // region get an unique weight.
  $weight_delta = round(count($blocks) / 2);

  // Build form tree
  $form = array(
    '#action' => arg(4) ? url('admin/build/block_instances/'. $theme_key) : url('admin/build/block_instances/sort'),
    '#tree' => TRUE,
  );

  foreach ($blocks as $i => $block) {
    $key = $block['block_instance'];
    $form[$key]['module'] = array(
      '#type' => 'value',
      '#value' => $block['module'],
    );
    $form[$key]['delta'] = array(
      '#type' => 'value',
      '#value' => $block['delta'],
    );
    $form[$key]['info'] = array(
      '#value' => check_plain($block['info'])
    );
    $form[$key]['theme'] = array(
      '#type' => 'hidden',
      '#value' => $theme_key
    );
    $form[$key]['block_instance'] = array(
      '#type' => 'hidden',
      '#value' => check_plain($block['block_instance'])
    );
    $form[$key]['weight'] = array(
      '#type' => 'weight',
      '#default_value' => $block['weight'],
      '#delta' => $weight_delta,
    );
    $form[$key]['region'] = array(
      '#type' => 'select',
      '#default_value' => $block['region'],
      '#options' => $block_regions,
    );
    $form[$key]['configure'] = array('#value' => l(t('configure'), 'admin/build/block_instances/list/'. $block['block_instance'] .'/edit'));
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save blocks'),
  );

  return $form;
}

/**
 * Process main blocks administration form submission.
 */
function block_instances_admin_display_form_submit($form, &$form_state) {
  $settings = array();

  /*
   * @TODO:
   *
   * Implement a better algorithm here. Blocks instances should be numbered with
   * weights from 100 to 100.
   *
   * Make the best effort to change only the weights of the block instances that
   * are not only in code, and even better, only change the block instance that
   * was moved from its original region/position
   */

  ctools_include('export');

  foreach ($form_state['values'] as $block) {
    if (is_array($block)) {
      $region = $block['region'];
      $name = $block['block_instance'];
      $weight = $block['weight'];

      $block_instance = ctools_export_crud_load('block_instances', $name);
      $block_instance->region = $region;
      $block_instance->weight = intval($weight);

      ctools_export_crud_save('block_instances', $block_instance);
    }
  }

  drupal_set_message(t('The block settings have been updated.'));
  block_instances_invalidate_cache();
}

/**
 * Helper function for sorting blocks on admin/build/block.
 *
 * Active blocks are sorted by region, then by weight.
 * Disabled blocks are sorted by name.
 */
function _block_instances_compare($a, $b) {
  global $theme_key;
  static $regions;

  // We need the region list to correctly order by region.
  if (!isset($regions)) {
    $regions = array_flip(array_keys(system_region_list($theme_key)));
    $regions[BLOCK_REGION_NONE] = count($regions);
  }

  // Separate enabled from disabled.
  $status = $b['status'] - $a['status'];
  if ($status) {
    return $status;
  }
  // Sort by region (in the order defined by theme .info file).
  if ((!empty($a['region']) && !empty($b['region'])) && ($place = ($regions[$a['region']] - $regions[$b['region']]))) {
    return $place;
  }
  // Sort by weight.
  $weight = $a['weight'] - $b['weight'];
  if ($weight) {
    return $weight;
  }
  // Sort by title.
  return strcmp($a['info'], $b['info']);
}

/**
 * Process variables for block-admin-display.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $form
 *
 * @see block-admin-display.tpl.php
 * @see theme_block_admin_display()
 */
function template_preprocess_block_instances_admin_display_form(&$variables) {
  global $theme_key;

  $block_regions = system_region_list($theme_key);
  $variables['throttle'] = module_exists('throttle');
  $variables['block_regions'] = $block_regions + array(BLOCK_REGION_NONE => t('Disabled'));

  foreach ($block_regions as $key => $value) {
    // Highlight regions on page to provide visual reference.
    drupal_set_content($key, '<div class="block-region">'. $value .'</div>');
    // Initialize an empty array for the region.
    $variables['block_listing'][$key] = array();
  }

  // Initialize disabled blocks array.
  $variables['block_listing'][BLOCK_REGION_NONE] = array();

  // Set up to track previous region in loop.
  $last_region = '';
  foreach (element_children($variables['form']) as $i) {
    $block = &$variables['form'][$i];

    // Only take form elements that are blocks.
    if (isset($block['info'])) {
      // Fetch region for current block.
      $region = $block['region']['#default_value'];

      // Set special classes needed for table drag and drop.
      $variables['form'][$i]['region']['#attributes']['class'] = 'block-region-select block-region-'. $region;
      $variables['form'][$i]['weight']['#attributes']['class'] = 'block-weight block-weight-'. $region;

      $variables['block_listing'][$region][$i]->row_class = isset($block['#attributes']['class']) ? $block['#attributes']['class'] : '';
      $variables['block_listing'][$region][$i]->block_modified = isset($block['#attributes']['class']) && strpos($block['#attributes']['class'], 'block-modified') !== FALSE ? TRUE : FALSE;
      $variables['block_listing'][$region][$i]->block_title =  drupal_render($block['info']);
      $variables['block_listing'][$region][$i]->region_select = drupal_render($block['region']) . drupal_render($block['theme']);
      $variables['block_listing'][$region][$i]->weight_select = drupal_render($block['weight']);
      $variables['block_listing'][$region][$i]->throttle_check = $variables['throttle'] ? drupal_render($block['throttle']) : '';
      $variables['block_listing'][$region][$i]->configure_link = drupal_render($block['configure']);
      $variables['block_listing'][$region][$i]->delete_link = !empty($block['delete']) ? drupal_render($block['delete']) : '';
      $variables['block_listing'][$region][$i]->printed = FALSE;

      $last_region = $region;
    }
  }

  $variables['form_submit'] = drupal_render($variables['form']);
}
