<?php

/**
 * @file
 * Drush integration of block instances.
 */

/**
 * Adds a cache clear option for block instances.
 */
function block_instances_drush_cache_clear(&$types) {
  $types['block_instances'] = 'block_instances_invalidate_cache';
}
