<?php

/**
 * Implementation of hook_install().
 */
function block_instances_install() {
  drupal_install_schema('block_instances');
}

/**
 * Implementation of hook_uninstall().
 */
function block_instances_uninstall() {
  drupal_uninstall_schema('block_instances');
}

/**
 * Implementation of hook_schema().
 */
function block_instances_schema() {
  $schema['block_instances'] = array(
    'description' => 'Stores the general data for block instances.',
    'fields' => array(
      'vid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The set ID, defined by the database.',
        'no export' => TRUE,
      ),
      'admin_title' => array(
        'type' => 'varchar',
        'length' => '32',
        'default' => '',
        'not null' => TRUE,
        'description' => 'An human name for this block instance.',
      ),
      'machine_name' => array(
        'type' => 'varchar',
        'length' => '32',
        'default' => '',
        'not null' => TRUE,
        'description' => 'The unique name for this block instance.',
      ),
      'region' => array(
        'type' => 'varchar',
        'length' => '32',
        'default' => '',
        'not null' => TRUE,
        'description' => 'The region where this block will be displayed.',
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
        'description' => 'Block instance weight within region.',
      ),
      'settings' => array(
        'type' => 'text',
        'description' => 'Conditions and extra settings that have to be meet to display this block.',
        'serialize' => TRUE,
      ),
    ),
    'primary key' => array('vid'),
    'unique key' => array(
      'machine_name' => array('name', 'machine_name')
    ),
    'indexes' => array(
      'region' => array('region', 'weight'),
    ),
    'export' =>  array(
      'key' => 'machine_name',
      'identifier' => 'block_instances',
      'export module' => 'block_instances',
      'admin_title' => 'admin_title',
      'api' => array(
        'owner' => 'block_instances',
        'api' => 'block_instances_default',
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
  );

  $schema['cache_block_instances'] = drupal_get_schema_unprocessed('system', 'cache');
  $schema['cache_block_instances']['description'] = 'Cache table for the Block module to store blocks instances loaded from both database and code.';

  return $schema;
}
