Drupal.behaviors.block_instances = function() {

  // Dynamically display the Views argument box if the selected block
  // is provided by a view.
  display_argument_box(null);
  $("#edit-block").change(display_argument_box);
  $("#edit-block").keypress(display_argument_box);

  function display_argument_box(event) {
    var selected_block = $(this).val();
    if (selected_block.indexOf('views:') == 0) {
      $("#edit-view-arguments-wrapper").show();
    }
    else {
      $("#edit-view-arguments-wrapper").hide();
    }
  }

  // Copied from features.js

  // Export form machine-readable JS
  $('.feature-name:not(.processed)').each(function() {
    $('.feature-name')
      .addClass('processed')
      .after(' <div><small class="feature-module-name-suffix">&nbsp;</small></div>');
    if ($('.feature-module-name').val() === $('.feature-name').val().toLowerCase().replace(/[^a-z0-9]+/g, '_').replace(/_+/g, '_') || $('.feature-module-name').val() === '') {
      $('.feature-module-name').parents('.form-item').hide();
      $('.feature-name').keyup(function() {
        var machine = $(this).val().toLowerCase().replace(/[^a-z0-9]+/g, '_').replace(/_+/g, '_');
        if (machine !== '_' && machine !== '') {
          $('.feature-module-name').val(machine);
          $('.feature-module-name-suffix').empty().append(' Machine name: ' + machine + ' [').append($('<a href="#">'+ Drupal.t('Edit') +'</a>').click(function() {
            $('.feature-module-name').parents('.form-item').show();
            $('.feature-module-name-suffix').hide();
            $('.feature-name').unbind('keyup');
            return false;
          })).append(']');
        }
        else {
          $('.feature-module-name').val(machine);
          $('.feature-module-name-suffix').text('');
        }
      });
      $('.feature-name').keyup();
    }
  });
};
