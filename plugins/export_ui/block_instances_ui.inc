<?php

/**
 * @file
 * A Ctools Export UI plugin for Block Instances.
 */

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'block_instances',
  'access' => 'configure block instances',
  'menu' => array(
    'menu item' => 'block_instances',
    'menu title' => 'Block Instances',
    'menu description' => 'Administer when and where blocks are displayed',
  ),

  'title singular' => t('block instance'),
  'title plural' => t('block instances'),
  'title singular proper' => t('Block instances'),
  'title plural proper' => t('Block instances'),

  'form' => array(
    'settings' => 'block_instances_ctools_settings_form',
    'submit' => 'block_instances_ctools_submit_form',
  ),

  'handler' => array(
     'class' => 'ctools_export_ui_block_instances',
     'parent' => 'ctools_export_ui',
   ),
);

/**
 * Define the preset add/edit form.
 */
function block_instances_ctools_settings_form(&$form, &$form_state) {
  global $theme_key, $custom_theme;
  init_theme();

  $block_instance = $form_state['item'];

  if (empty($block_instance->machine_name)) {
    block_instances_default($block_instance);
  }

  $part_access = array_filter($block_instance->settings['settings']['restrict_parts']);
  $non_restricted_user = user_access('administers block instance');
  $skip_check = $non_restricted_user || count($part_access) == 0;

  drupal_add_css(drupal_get_path('module', 'block_instances') . '/plugins/export_ui/block_instances.css');
  drupal_add_js(drupal_get_path('module', 'block_instances') . '/plugins/export_ui/block_instances.js');
  ctools_include('dependent');

  $form['info']['admin_title']['#prefix'] = '<div class ="block-instance-left"><h2>' . t('Select the block and the region') . '</h2>';
  $form['info']['admin_title']['#attributes'] = array('class' => 'feature-name');
  $form['info']['admin_title']['#description'] = t('Example: Recent comments');
  $form['info']['admin_title']['#required'] = TRUE;
  $form['info']['admin_title']['#access'] = $skip_check || !empty($part_access['admin_title']);

  $form['info']['admin_title']['#size'] = 32;
  $form['info']['machine_name']['#size'] = 32;
  $form['info']['machine_name']['#title'] = t('Machine name');
  $form['info']['machine_name']['#attributes'] = array('class' => 'feature-module-name');

  foreach (module_implements('block') as $module) {
    foreach (module_invoke($module, 'block', 'list') as $id => $block) {
      $blocks["$module:$id"] = filter_xss($block['info']);
    }
  }

  $exclude_blocks = variable_get('block_instances_exlude_blocks', array());
  foreach ($exclude_blocks as $exclude_block) {
    unset($blocks[$exclude_block]);
  }

  $form['block'] = array(
    '#type' => 'select',
    '#title' => t('Block to display'),
    '#options' => $blocks,
    '#default_value' => $block_instance->settings['block'],
    '#description' => t('Which block will be displayed.'),
    '#access' => $skip_check || !empty($part_access['change_block']),
  );

  $regions = system_region_list($theme_key) + array(BLOCK_REGION_NONE => '- '. t('Hide block') .' -') ;

  if (!$non_restricted_user) {
    if (count($block_instance->settings['settings']['restrict_regions'])) {
      foreach ($regions as $region => $value) {
        if (empty($block_instance->settings['settings']['restrict_regions'][$region])) {
          unset($regions[$region]);
        }
      }
    }
  }

  $form['region'] = array(
    '#type' => 'select',
    '#title' => t('Region to display this block'),
    '#options' => $regions,
    '#default_value' => isset($block_instance->region) ? $block_instance->region : $regions[0],
    '#description' => t('Where will this block be rendered'),
    '#access' => $skip_check || !empty($part_access['change_region']),
  );

  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 50,
    '#default_value' => $block_instance->weight,
    '#description' => t('The weight for this block instance.'),
    '#access' => $skip_check || !empty($part_access['change_weight']),
  );

  $form['view_arguments'] = array(
    '#type' => 'textfield',
    '#title' => t('View arguments'),
    '#description' => t('Separate arguments with a / as though they were a URL path.'),
    '#default_value' => $block_instance->settings['view_arguments'],
    '#size' => 40,
    '#access' => $skip_check || !empty($part_access['change_view_arguments']),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('This description will be used in sort block instance screen.'),
    '#default_value' => $block_instance->settings['description'],
    '#suffix' => "</div>",
  );

  $form['override_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Override block title'),
    '#default_value' => $block_instance->settings['override_title'],
    '#prefix' => '<div class ="block-instance-right"><h2>' . t('Block title settings') . '</h2>',
    '#access' => $skip_check || !empty($part_access['override_title']),
  );
  $form['override_title_settings'] = array(
    '#type' => 'textfield',
    '#title' => t('New block title'),
    '#default_value' => $block_instance->settings['override_title_settings'],
    '#description' => t('Provide a new title for this block, or leave it in blank to hide the block title'),
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array(
      'edit-override-title' => array(1)
    ),
    '#prefix' => '<div class ="block-instances-item-expanded">',
    '#suffix' => '</div></div>',
    '#access' => $skip_check || !empty($part_access['override_title']),
  );

  $form['require_all_conditions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require all conditions'),
    '#description' => t('If checked, all the conditions defined below has to be TRUE to display the block.'),
    '#default_value' => $block_instance->settings['require_all_conditions'],
    '#prefix' => '<div class ="block-instance-right"><h2>' . t('Block visibility') . '</h2>',
  );

  $form['conditions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Conditions'),
    '#collapsible' => TRUE,
    '#description' => t('The following conditions determine if the block will be rendered or not.'),
    '#access' => $skip_check || !empty($part_access['change_conditions']),
  );

  $form['conditions']['site_wide'] = array(
    '#type' => 'checkbox',
    '#title' => t('Site wide'),
    '#description' => t('Display this block in all pages of the site.'),
    '#default_value' => $block_instance->settings['conditions']['site_wide'],
    '#collapsible' => TRUE,
  );

  $form['conditions']['node_types'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display on nodes'),
    '#description' => t('This block will be displayed when you are seeing content.'),
    '#default_value' => $block_instance->settings['conditions']['node_types'],
  );

  $form['conditions']['node_types_settings'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#default_value' => $block_instance->settings['conditions']['node_types_settings'],
    '#options' => array_map('check_plain', node_get_types('names')),
    '#description' => t('Select content types where this block will be visible.'),
    '#prefix' => '<div id="edit-node-types-settings-wrapper" class ="block-instances-item-expanded"><div id="node-types-settings">',
    '#suffix' => '</div></div>',
    '#process' => array('ctools_dependent_process', 'expand_checkboxes'),
    '#dependency' => array(
      'edit-node-types' => array(1)
    ),
  );

  $path_description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

  $form['conditions']['this_paths'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display on certains paths'),
    '#description' => t('This block will be displayed when you are in a certain path.'),
    '#default_value' => $block_instance->settings['conditions']['this_paths'],
  );

  $form['conditions']['this_paths_settings'] = array(
    '#type' => 'textarea',
    '#title' => t('Display on this paths'),
    '#default_value' => $block_instance->settings['conditions']['this_paths_settings'],
    '#description' => $path_description,
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array(
      'edit-this-paths' => array(1)
    ),
    '#prefix' => '<div class ="block-instances-item-expanded">',
    '#suffix' => '</div>'
  );

  $form['conditions']['not_this_paths'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display on not certains paths'),
    '#description' => t('This block will be displayed when users are not on this paths.'),
    '#default_value' => $block_instance->settings['conditions']['not_this_paths'],
  );

  $form['conditions']['not_this_paths_settings'] = array(
    '#type' => 'textarea',
    '#title' => t('Do not display on this paths'),
    '#default_value' => $block_instance->settings['conditions']['not_this_paths_settings'],
    '#description' => $path_description,
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array(
      'edit-not-this-paths' => array(1)
    ),
    '#prefix' => '<div class ="block-instances-item-expanded">',
    '#suffix' => '</div>'
  );

  $form['conditions']['roles'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display when user has special role'),
    '#description' => t('This block will be displayed when users are not on this paths.'),
    '#default_value' => $block_instance->settings['conditions']['roles'],
  );

  $result = db_query("SELECT r.rid, r.name FROM {role} r ORDER BY r.name");
  while ($obj = db_fetch_object($result)) {
    $roles[$obj->rid] = check_plain($obj->name);
  }

  $form['conditions']['roles_settings'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User roles'),
    '#prefix' => '<div id="edit-roles-settings-wrapper" class ="block-instances-item-expanded"><div id="roles-settings">',
    '#suffix' => '</div></div>',
    '#options' => $roles,
    '#description' => t('Select which roles may have the user to see this block.'),
    '#process' => array('ctools_dependent_process', 'expand_checkboxes'),
    '#dependency' => array(
      'edit-roles' => array(1)
    ),
    '#default_value' => $block_instance->settings['conditions']['roles_settings'],
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Users with <strong>administer block instances</strong> permissions can skip this restrictions'),
  );

  $form['settings']['restrict_regions'] = array(
    '#type' => count($regions) > 5 ? 'select' : 'checkboxes',
    '#multiple' => TRUE,
    '#size' => 5,
    '#title' => t('Restrict regions'),
    '#options' => $regions,
    '#description' => t('Select from which regions will be able to choose the user. If none is checked, all regions will be available.'),
    '#default_value' => $block_instance->settings['settings']['restrict_regions'],
    '#prefix' => '<div class ="block-instance-left">',
  );

  $form['settings']['restrict_edit'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Restrict editors'),
    '#options' => $roles,
    '#description' => t('Select who can edit this block instance.'),
    '#default_value' => $block_instance->settings['settings']['restrict_edit'],
    '#suffix' => '</div>',
  );

  $form['settings']['restrict_parts'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Restrict items to edit'),
    '#options' => array(
      'change_admin_title' => t('Change administrative title'),
      'override_title' => t('Override title'),
      'change_region' => t('Change region'),
      'change_block' => t('Change block'),
      'change_view_arguments' => t('Change view arguments'),
      'change_visibility' => t('Change visibility'),
    ),
    '#description' => t('Select what items can be edited for this block instance.'),
    '#default_value' => $block_instance->settings['settings']['restrict_parts'],
    '#prefix' => '<div class ="block-instance-right">',
    '#sufix' => '</div></div>',
  );
}

/**
 * Implementation of hook_block_instances_default().
 */
function block_instances_default(&$block_instance) {
  $block_instance = new StdClass;
  $block_instance->weight = 0;
  $block_instance->settings['conditions'] = array(
    'node_types' => FALSE,
    'node_types_settings' => array(),

    // Users usually expect to see their blocks when create the instance
    'site_wide' => TRUE,

    'front_page' => FALSE,

    'this_paths' => FALSE,
    'this_paths_settings' => '',

    'not_this_paths' => FALSE,
    'not_this_paths_settings' => '',

    'roles' => FALSE,
    'roles_settings' => array(),
  );

  $block_instance->settings['require_all_conditions'] = FALSE;
  $block_instance->settings['settings'] = array(
    'restrict_regions' => array(),
    'restrict_edit' => array(),
    'restrict_parts' => array(),
  );

  return $block_instance;
}

/**
 * Prepare values for saving by ctools export UI.  Pushing the data to the database is handled by ctools Export UI.
 */
function block_instances_ctools_submit_form($form, &$form_state) {
  $form_state['item']->settings['block'] = $form_state['values']['block'];
  if (strpos($form_state['values']['block'], 'views:') !== FALSE) {
    $form_state['item']->settings['is_view_block'] = TRUE;
    $form_state['item']->settings['view_arguments'] = $form_state['values']['view_arguments'];
  }
  else {
    $form_state['item']->settings['is_view_block'] = FALSE;
  }
  $form_state['item']->settings['override_title'] = $form_state['values']['override_title'];
  $form_state['item']->settings['override_title_settings'] = $form_state['values']['override_title_settings'];
  $form_state['item']->settings['description'] = $form_state['values']['description'];
  $form_state['item']->settings['require_all_conditions'] = $form_state['values']['require_all_conditions'];

  foreach (element_children($form['conditions']) as $key) {
    $form_state['item']->settings['conditions'][$key] = $form_state['values'][$key];
  }
  foreach (element_children($form['settings']) as $key) {
    $form_state['item']->settings['settings'][$key] = $form_state['values'][$key];
  }
  block_instances_invalidate_cache();
}
