<?php

class ctools_export_ui_block_instances extends ctools_export_ui {

  function access($op, $block_instances) {
    if ($op != 'edit' && $op != 'delete') {
      return parent::access($op, $block_instances);
    }
    else {
      if (!empty($block_instances->settings['settings']['restrict_edit'])) {
        if (!user_access('administer block instances')) {
          global $user;
          $allow_edit = FALSE;
          foreach ($user->roles as $id => $role) {
            if (!empty($block_instances->settings['settings']['restrict_edit'][$rid])) {
              $allow_edit = TRUE;
              break;
            }
          }
          if (!$allow_edit) {
            if ($op == 'edit') {
              drupal_set_message(t("Sorry but you are not allowed to edit this block instance."), 'error', FALSE);  
            }
            else {
              drupal_set_message(t("Sorry but you are not allowed to delete this block instance."), 'error', FALSE);  
            }
            return FALSE;
          }
        }
      }
      return TRUE;
    }
  }

  function list_sort_options() {
    $options = parent::list_sort_options();
    $options['region'] = t('Region');
    return $options;
  }

  function list_build_row($item, &$form_state, $operations) {
    // Set up sorting
    $name = $item->{$this->plugin['export']['key']};

    // Note: $item->type should have already been set up by export.inc so
    // we can use it safely.
    switch ($form_state['values']['order']) {
      case 'disabled':
        $this->sorts[$name] = empty($item->disabled) . $name;
        break;
      case 'title':
        $this->sorts[$name] = $item->{$this->plugin['export']['admin_title']};
        break;
      case 'name':
        $this->sorts[$name] = $name;
        break;
      case 'storage':
        $this->sorts[$name] = $item->type . $name;
        break;
      case 'region':
        $this->sorts[$name] = $item->region;
        break;
    }

    $this->rows[$name]['data'] = array();
    $this->rows[$name]['class'] = !empty($item->disabled) ? 'ctools-export-ui-disabled' : 'ctools-export-ui-enabled';

    // If we have an admin title, make it the first row.
    if (!empty($this->plugin['export']['admin_title'])) {
      $this->rows[$name]['data'][] = array('data' => check_plain($item->{$this->plugin['export']['admin_title']}), 'class' => 'ctools-export-ui-title');
    }
    $this->rows[$name]['data'][] = array('data' => check_plain($name), 'class' => 'ctools-export-ui-name');
    $this->rows[$name]['data'][] = array('data' => check_plain($item->region), 'class' => 'ctools-export-ui-region');
    $this->rows[$name]['data'][] = array('data' => check_plain($item->type), 'class' => 'ctools-export-ui-storage');
    $this->rows[$name]['data'][] = array('data' => theme('links', $operations), 'class' => 'ctools-export-ui-operations');

    // Add an automatic mouseover of the description if one exists.
    if (!empty($this->plugin['export']['admin_description'])) {
      $this->rows[$name]['title'] = $item->{$this->plugin['export']['admin_description']};
    }
  }

  /**
   * Provide the table header.
   *
   * If you've added columns via list_build_row() but are still using a
   * table, override this method to set up the table header.
   */
  function list_table_header() {
    $header = array();
    if (!empty($this->plugin['export']['admin_title'])) {
      $header[] = array('data' => t('Title'), 'class' => 'ctools-export-ui-title');
    }

    $header[] = array('data' => t('Name'), 'class' => 'ctools-export-ui-name');
    $header[] = array('data' => t('Region'), 'class' => 'ctools-export-ui-region');
    $header[] = array('data' => t('Storage'), 'class' => 'ctools-export-ui-storage');
    $header[] = array('data' => t('Operations'), 'class' => 'ctools-export-ui-operations');

    return $header;
  }

  function list_form(&$form, &$form_state) {
    parent::list_form($form, $form_state);
    global $theme_key, $custom_theme;

    // If non-default theme configuration has been selected, set the custom theme.
    $custom_theme = isset($theme) ? $theme : variable_get('theme_default', 'garland');
    init_theme();

    $theme_regions = system_region_list($theme_key) + array(BLOCK_REGION_NONE => '<'. t('none') .'>');
    $all = array('all' => t('- All -'));
    $form['top row']['region'] = array(
      '#type' => 'select',
      '#title' => t('Region'),
      '#options' => $all + $theme_regions,
      '#default_value' => 'all',
    );
    $form['top row']['search']['#weight'] = 1;
  }

  function list_filter($form_state, $item) {
    parent::list_filter($form_state, $item);
    if ($form_state['values']['region'] != 'all' && $form_state['values']['region'] != $item->region) {
      return TRUE;
    }
  }

  function set_item_state($state, $js, $input, $item) {
    ctools_export_set_object_status($item, $state);
    block_instances_invalidate_cache();
    if (!$js) {
      drupal_goto(ctools_export_ui_plugin_base_path($this->plugin));
    }
    else {
      return $this->list_page($js, $input);
    }
  }

  /**
   * Page callback to delete an exportable item.
   */
  function delete_page($js, $input, $item) {
    $form_state = array(
      'plugin' => $this->plugin,
      'object' => &$this,
      'ajax' => $js,
      'item' => $item,
      'op' => $item->export_type & EXPORT_IN_CODE ? 'revert' : 'delete',
      'rerender' => TRUE,
      'no_redirect' => TRUE,
    );

    ctools_include('form');

    $output = ctools_build_form('ctools_export_ui_delete_confirm_form', $form_state);
    if (!empty($form_state['executed'])) {
      ctools_export_crud_delete($this->plugin['schema'], $item);
      block_instances_invalidate_cache();
      $export_key = $this->plugin['export']['key'];
      $message = str_replace('%title', check_plain($item->{$export_key}), $this->plugin['strings']['confirmation'][$form_state['op']]['success']);
      drupal_set_message($message);
      drupal_goto(ctools_export_ui_plugin_base_path($this->plugin));
    }

    return $output;
  }
}
